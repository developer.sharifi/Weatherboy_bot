from telegram import Update, KeyboardButton, ReplyKeyboardMarkup, ReplyKeyboardRemove, InlineKeyboardButton, \
    InlineKeyboardMarkup
from telegram.ext import CallbackContext

from constants import CURRENT_WEATHER_CALLBACK_DATA, FIVE_DAY_FORECAST_CALLBACK_DATA, CHANGE_MY_LOCATION_CALLBACK_DATA, \
    CURRENT_WEATHER_STATE, FORECAST_STATE, CITY_NOT_FOUND
from repositories.geolocation_repo import GeoLocationRepo
from repositories.user_repo import UserRepo
from repositories.weather_repo import WeatherRepo

current_or_forecast_state = None


def start(update: Update, context: CallbackContext):
    print('Method: start')
    submit_my_location(context, update)


def submit_my_location(context, update):
    print('Method: submit_my_location')
    mylocation_keyboard = [
        [KeyboardButton('submit my current location', request_location=True)]
    ]
    mylocation_keyboard_markup = ReplyKeyboardMarkup(keyboard=mylocation_keyboard)
    context.bot.send_message(
        # can't i write this better?
        chat_id=(update.callback_query or update).message.chat_id,
        text="Submit your location to get weather forecasts daily.\n" +
             "To submit another place, use attach button, select a location and " +
             "then send your selected location.",
        reply_markup=mylocation_keyboard_markup)


def location(update: Update, context: CallbackContext):
    print('Method: location')
    chat_id = update.message.chat_id
    latitude = update.message.location.latitude
    longitude = update.message.location.longitude
    geo_location_repo = GeoLocationRepo()
    timezone = geo_location_repo.get_timezone_from_coordinates(lat=latitude, long=longitude)
    user_repo = UserRepo()
    submitted_user = user_repo.submit_user(chat_id=chat_id, latitude=latitude, longitude=longitude, timezone=timezone)
    context.bot.send_message(chat_id=update.message.chat_id,
                             text="You'll get 5-day weather forecast for your location everyday on 6 o'clock.\n" +
                                  "Remember, you can change your location anytime through main menu.",
                             reply_markup=ReplyKeyboardRemove())
    main_menu(update=update, context=context)


def main_menu(update: Update, context: CallbackContext):
    print('Method: main_menu')
    main_menu_inline_keyboard = [
        [InlineKeyboardButton(text='Current weather', callback_data=CURRENT_WEATHER_CALLBACK_DATA)],
        [InlineKeyboardButton(text='5-day forecast', callback_data=FIVE_DAY_FORECAST_CALLBACK_DATA)],
        [InlineKeyboardButton(text='Change my location', callback_data=CHANGE_MY_LOCATION_CALLBACK_DATA)],
    ]
    main_menu_inline_keyboard_markup = InlineKeyboardMarkup(inline_keyboard=main_menu_inline_keyboard)

    # removing keyboard has potential to become a decorator
    context.bot.send_message(chat_id=(update.callback_query or update).message.chat_id,
                             text="main_menu",
                             reply_markup=ReplyKeyboardRemove())

    context.bot.send_message(chat_id=(update.callback_query or update).message.chat_id,
                             text="choose an option",
                             reply_markup=main_menu_inline_keyboard_markup)


def button(update: Update, context: CallbackContext):
    print('Method: button')
    query_data = update.callback_query.data
    global current_or_forecast_state
    if query_data == CURRENT_WEATHER_CALLBACK_DATA:
        current_or_forecast_state = CURRENT_WEATHER_STATE
        current_weather_callback(update=update, context=context)
    elif query_data == FIVE_DAY_FORECAST_CALLBACK_DATA:
        current_or_forecast_state = FORECAST_STATE
        five_day_forecast_callback(update=update, context=context)
    elif query_data == CHANGE_MY_LOCATION_CALLBACK_DATA:
        change_my_location_callback(update=update, context=context)


def current_weather_callback(update: Update, context: CallbackContext):
    print('Method: current_weather_callback')
    global current_or_forecast_state
    current_or_forecast_state = CURRENT_WEATHER_STATE
    context.bot.send_message(chat_id=(update.callback_query or update).message.chat_id,
                             text="Type name of the city you want it's weather.")


def five_day_forecast_callback(update: Update, context: CallbackContext):
    print('Method: five_day_forecast_callback')
    global current_or_forecast_state
    current_or_forecast_state = FORECAST_STATE
    context.bot.send_message(chat_id=(update.callback_query or update).message.chat_id,
                             text="Type name of the city you want it's 5-day forecast.")


def weather_query(update: Update, context: CallbackContext):
    print('Method: weather_query')
    weather_repo = WeatherRepo()
    global current_or_forecast_state
    city = update.message.text or ''
    if city != '':
        if current_or_forecast_state == CURRENT_WEATHER_STATE:
            weather = weather_repo.get_current_weather_by_city(city=city) or CITY_NOT_FOUND
            context.bot.send_message(chat_id=update.message.chat_id,
                                     text="current weather for city {city}:\n\n{current_weather}".format(
                                         city=city, current_weather=str(weather)))

        elif current_or_forecast_state == FORECAST_STATE:
            weather_list = weather_repo.get_five_day_forecast_by_city(city=city) or [CITY_NOT_FOUND]
            context.bot.send_message(chat_id=update.message.chat_id,
                                     text="5-day forecast for city {city}:\n\n{five_day_forecast}".format(
                                         city=update.message.text,
                                         five_day_forecast=''.join(str(weather) + '\n\n' for weather in weather_list)))

    current_or_forecast_state = None
    main_menu(update=update, context=context)


def change_my_location_callback(update: Update, context: CallbackContext):
    print('Method: change_my_location_callback')
    submit_my_location(update=update, context=context)