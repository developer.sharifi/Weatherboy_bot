from telegram.ext import CommandHandler, MessageHandler, Filters, CallbackQueryHandler, Updater

from method_hub import start, location, main_menu, button, current_weather_callback, five_day_forecast_callback, \
    weather_query, change_my_location_callback


class Dispatcher:
    def __init__(self, updater: Updater):
        self.dispatcher = updater.dispatcher
        self.__set_handlers()

    def __set_handlers(self):
        start_handler = CommandHandler(command="start", callback=start)
        self.dispatcher.add_handler(handler=start_handler)

        location_handler = MessageHandler(filters=Filters.location, callback=location)
        self.dispatcher.add_handler(location_handler)

        main_menu_command_handler = CommandHandler(command='main_menu', callback=main_menu)
        self.dispatcher.add_handler(handler=main_menu_command_handler)

        button_handler = CallbackQueryHandler(button)
        self.dispatcher.add_handler(button_handler)

        current_weather_handler = CommandHandler(command='current_weather', callback=current_weather_callback)
        self.dispatcher.add_handler(current_weather_handler)

        five_day_forecast_handler = CommandHandler(command='forecast_weather', callback=five_day_forecast_callback)
        self.dispatcher.add_handler(five_day_forecast_handler)

        weather_query_handler = MessageHandler(Filters.text, weather_query)
        self.dispatcher.add_handler(weather_query_handler)

        change_location_command_handler = CommandHandler('change_location', callback=change_my_location_callback)
        self.dispatcher.add_handler(change_location_command_handler)
