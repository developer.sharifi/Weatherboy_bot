from sqlalchemy import Column, Integer, Float, String

from db.db_config import Base


class DBUser(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    chat_id = Column(Integer, unique=True)
    latitude = Column(Float, nullable=True)
    longitude = Column(Float, nullable=True)
    timezone = Column(String, nullable=True)

    def __init__(self,
                 chat_id: int,
                 latitude: float,
                 longitude: float,
                 timezone: str):
        self.chat_id = chat_id
        self.latitude = latitude
        self.longitude = longitude
        self.timezone = timezone
