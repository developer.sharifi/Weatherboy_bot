CITY_NOT_FOUND = 'City not found!'
CURRENT_WEATHER_STATE = 'current_weather'
FORECAST_STATE = 'forecast'

# keyboard callback datas
CURRENT_WEATHER_CALLBACK_DATA = 'current_weather'
FIVE_DAY_FORECAST_CALLBACK_DATA = 'five_day_forecast'
CHANGE_MY_LOCATION_CALLBACK_DATA = 'change_my_location'
MAIN_MENU_CALLBACK_DATA = 'main_menu'
