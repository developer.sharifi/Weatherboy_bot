import datetime

import pytz
import tzlocal
from telegram.ext import CallbackContext

from constants import CITY_NOT_FOUND
from repositories.user_repo import UserRepo
from repositories.weather_repo import WeatherRepo


class JobTimer:
    def __init__(self, updater):
        self.updater = updater
        self.__set_timer()

    def __set_timer(self):
        self.updater.job_queue.run_daily(callback=self.five_day_forecast_daily_job,
                                         time=(datetime.datetime.now() + datetime.timedelta(seconds=5)).time())

    def five_day_forecast_one_time_job(self, context: CallbackContext):
        print('Method: five_day_forecast_one_time_job')
        user = context.job.context
        weather_repo = WeatherRepo()
        weather_list = \
            weather_repo.get_five_day_forecast_by_lat_long(latitude=user.latitude, longitude=user.longitude) \
            or [CITY_NOT_FOUND]

        if weather_list[0] != CITY_NOT_FOUND:
            city = weather_list[0].city
        else:
            city = CITY_NOT_FOUND
        context.bot.send_message(chat_id=user.chat_id,
                                 text="5-day forecast near your place at city {city}:\n\n{five_day_forecast}".format(
                                     city=city,
                                     five_day_forecast=''.join(str(weather) + '\n\n' for weather in weather_list)))

    def five_day_forecast_daily_job(self, context: CallbackContext):
        print('Method: five_day_forecast_one_time_job')
        user_repo = UserRepo()
        users = user_repo.get_all_users()
        job_execution_time = datetime.time(hour=6, minute=0)
        for user in users:
            local_timezone = tzlocal.get_localzone()
            user_timezone = pytz.timezone(user.timezone)
            user_now_datetime = datetime.datetime.now(tz=user_timezone)
            user_today = user_now_datetime.date()
            user_tomorrow = user_today + datetime.timedelta(days=1)
            if user_now_datetime.time() > job_execution_time:
                # it should be tomorrow
                execution_datetime_in_user_timezone = datetime.datetime(year=user_tomorrow.year,
                                                                        month=user_tomorrow.month,
                                                                        day=user_tomorrow.day,
                                                                        hour=job_execution_time.hour,
                                                                        minute=job_execution_time.minute,
                                                                        second=job_execution_time.second,
                                                                        tzinfo=user_timezone)
            else:
                # it should be today
                execution_datetime_in_user_timezone = datetime.datetime(year=user_today.year,
                                                                        month=user_today.month,
                                                                        day=user_today.day,
                                                                        hour=job_execution_time.hour,
                                                                        minute=job_execution_time.minute,
                                                                        second=job_execution_time.second,
                                                                        tzinfo=user_timezone)

            offset_aware_when = execution_datetime_in_user_timezone.astimezone(local_timezone)
            # python-telegram-bot library needs a naive datetime
            offset_naive_when = offset_aware_when.replace(tzinfo=None)
            # it seems we have an additional 4 minutes error in time conversion :|
            offset_naive_when = offset_naive_when - datetime.timedelta(minutes=4)

            self.updater.job_queue.run_once(callback=self.five_day_forecast_one_time_job,
                                            when=offset_naive_when,
                                            context=user)
