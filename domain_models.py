from datetime import datetime


class Wind:
    def __init__(self,
                 speed: float,
                 degree: float):
        self.speed = speed
        self.degree = degree

    def __str__(self):
        return ("speed: {speed}\t" +
                "degree: {degree}").format(
            speed=self.speed,
            degree=self.degree
        )


class Weather:
    def __init__(self,
                 dtime: datetime,
                 city: str,
                 status: str,
                 temperature: float,
                 wind: Wind,
                 humidity: float):
        self.dtime = dtime
        self.city = city
        self.status = status
        self.temperature = temperature
        self.wind = wind
        self.humidity = humidity

    def __str__(self):
        return ("time: {time}\n" +
                "city: {city}\n" +
                "status: {status}\n" +
                "temperature: {temperature}\n" +
                "wind: {wind}\n" +
                "humidity: {humidity}").format(
            time=self.dtime.strftime("%m/%d/%Y, %H:%M:%S"),
            city=self.city,
            status=self.status,
            temperature=self.temperature,
            wind=str(self.wind),
            humidity=self.humidity
        )


class User:
    def __init__(self,
                 id: int,
                 chat_id: int,
                 latitude: float,
                 longitude: float,
                 timezone: str):
        self.id = id
        self.chat_id = chat_id
        self.latitude = latitude
        self.longitude = longitude
        self.timezone = timezone
